package com.cm.task;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@Slf4j
public class PushGitee {

    @Scheduled(cron = "0 0/5 * * * ?")
    public void task() {

        String url = "https://api.github.com/repos/LeibNici/sync/actions/workflows/25869843/dispatches";

        String auth = "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";

        Map<String, Object> body = new HashMap<>();
        body.put("ref", "main");
        body.put("inputs", new Object());
        String bodyString = JSON.toJSONString(body);

        HttpResponse response = HttpRequest.post(url)
                .bearerAuth(auth)
                .header("Accept", "application/vnd.github.v3+json")
                .body(bodyString)
                .execute();

        if (response.getStatus() == 204){
            log.info("远程调用成功");
        }
    }

}
